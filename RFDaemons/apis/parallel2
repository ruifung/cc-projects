--[[

parallel2 - a better parallel module

Version 2 by Lambda Fairy

See <https://github.com/lfairy/lunatic> for updates.

]]

_VERSION = 2

yield = (os and os.pullEvent) or coroutine.yield

function group (...)
    local args = {...}
    if #args == 1 and type(args[1]) == 'table' then
        -- group({alice = ..., bob = ...})
        return Context:new(args[1])
    else
        -- group(alice, bob)
        return Context:new(args)
    end
end

Context = {}

function Context:new (functions)
    local o = {}
    setmetatable(o, self)
    self.__index = self

    -- Add each function to the table
    o.threads = {}
    for k, v in pairs(functions) do
        o.threads[k] = o:_wrapFunction(v)
    end

    -- Run the functions one step so we know what events they want
    o.eventFilters = {}
    o:feed(nil)

    return o
end

function Context:_wrapFunction(f)
    return coroutine.create(function () f(self) end)
end

function Context:run (limit)
    if limit == nil then
        limit = 0
    elseif limit < 0 then
        limit = limit + self.living
    end

    while self.living > limit do
        self:step()
    end
end

function Context:feed (...)
    local evData = {...}
    local evType = evData[1]

    -- Loop through all the coroutines
    for label, co in pairs(self.threads) do
        -- Only resume a coroutine if it needs the data we're holding
        -- (except for terminate events, which are always sent)
        local evFilter = self.eventFilters[co]
        if evFilter == nil or evFilter == evType or
                evType == 'terminate' then
            local ok, param = coroutine.resume(co, unpack(evData))

            if ok then
                self.eventFilters[co] = param
            else
                error(param)
            end
        end
    end

    -- Filter out all the dead ones
    self:_cullDead()
end

function Context:step ()
    return self:feed(yield())
end

Context.__call = Context.run -- Support nested contexts

function Context:_cullDead ()
    self.threads = filter(function (co)
        return coroutine.status(co) ~= 'dead'
    end, self.threads)
    self.living = countkeys(self.threads)
end

function Context:isAlive (key)
    return self.threads[key] and coroutine.status(self.threads[key]) ~= 'dead'
end

function Context:fork(a, b)
    if b == nil then
        -- fork(f)
        -- If we aren't given a key, assign one automatically
        local key = #self.threads + 1
        return self:_fork(key, a)
    else
        -- fork(key, f)
        return self:_fork(a, b)
    end
end

function Context:_fork (key, f)
    if self:isAlive(key) then
        error('thread ' .. key .. ' already running')
    else
        -- Adding an entry to a table while it's being iterated over
        -- causes undefined behavior, so we work on a copy of the table
        -- instead.
        -- See <http://www.lua.org/manual/5.1/manual.html#pdf-next>
        local new = copy(self.threads)
        new[key] = self:_wrapFunction(f)
        self.threads = threads
        return key
    end
end

function Context:kill (key)
    if self.threads[key] then
        self.threads[key] = nil
        return true
    else
        return false
    end
end

function copy (value)
    if type(value) == 'table' then
        local result = {}
        for k, v in pairs(value) do
            result[k] = v
        end
        setmetatable(result, getmetatable(value))
        return result
    else
        return value
    end
end

function countkeys (items)
    local n = 0
    for k, v in pairs(items) do
        n = n + 1
    end
    return n
end

function filter (f, list)
    local result = {}
    for k, v in pairs(list) do
        if f(v) then
            result[k] = v
        end
    end
    return result
end

function testParallel2 ()
    local function testa ()
        local r = coroutine.yield('ham')
        print(r)
    end
    local function testb ()
        for i = 1, 3 do
            local r = coroutine.yield('eggs')
            print('\t' .. r)
        end
    end
    local function testc (ctx)
        local r
        ctx:kill(4) -- testd
        for i = 1, 8 do
            r = coroutine.yield()
            print('\t\t' .. r)
        end
    end
    local function testd ()
        coroutine.yield() -- give other coroutines a chance to run
        error('this should never happen')
    end
    local ctx = group(testa, testb, testc, testd)
    while true do
        for k, v in pairs({ham = true, spam = true, eggs = true, ni = true}) do
            print('== ' .. ctx.living .. ' running ===========')
            print()
            ctx:feed(k)
            print()
            if ctx.living == 0 then
                return
            end
        end
    end
end

-- Uncomment to see heaps of scrolling text
--testParallel2()