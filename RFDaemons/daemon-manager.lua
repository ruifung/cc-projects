--[[
RFDaemons Daemon Manager
- Provides top level daemons
]]
local dummyFunction["dummy"] = function()
    while true do
        coroutine.yield()
    end
end
local context = parallel2.group(dummyFunction)
local tDaemons = {}
local api = {}
api["add"] = function(sName,fDaemon)
    if not tDaemons[sName] then
        tDaemons[sName] = fDaemon
        return true
    else
        return false
    end
end
api["remove"] == function(sName)
    if not tDaemons[sName] then
        if tDaemons[sName] == true then
            context:kill(sName)
        end
        tDaemons[sName] = nil
        return true
    else
        return false
    end
end
api["stop"] = function(sName)
    if not tDaemons[sName] == true then
        return false
    end
    if context:isAlive(sName) then
        tDaemons[sName] = context.threads[sName]
        return context:kill(sName)
    else
        tDaemons[sName] = nil
        return false
    end
end
api["start"] = function(sName)
    if context:isAlive(sName) then
        return false
    elseif not type(tDaemons[sName]) == "function"
        tDaemons[sName] = nil
        return false
    else
        context:fork(sName,tReturn[sName])
        tDaemons[sName] = true
        return true
    end
end
api["isRunning"] = function(sName)
    return context:isAlive(sName)
end
api["status"] = function()
    local tReturn = {}
    for k,v in pairs(tDaemons) do
        if type(v) == "function" then
            tReturn[k] = false
        elseif v == true then
            tReturn[k] = true
        end
    end
    return tReturn
end
_G["rfDaemons"] = api
context.run()