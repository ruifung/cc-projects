--[[
RFDaemons Loader
]]


local basePath = "RFCore"

--# load all api dependencies
for k,v in pairs(fs.list(basePath .. "/apis")) do
    local file = basePath .. "/apis/" .. v
    if not fs.isDir(file) then
       os.loadAPI(file)
    end
end

if pcall(function() 
    --# dependency check
    if not parallel2 then error("parallel2 api not loaded.") end
end) then
    --# let the parallel api load up the other routines
    os.startTimer(0)
    coroutine.yield('timer')

    --# top-level override
    os.queueEvent("modem_message")
    local p = _G.printError
    function _G.printError()
        _G.printError = p
        tEnv = {}
        tEnvMT = {__index=_G}
        setmetatable(tEnv,tEnvMT)
        os.run(tEnv,"basePath/daemon-manager.lua")
    end
end